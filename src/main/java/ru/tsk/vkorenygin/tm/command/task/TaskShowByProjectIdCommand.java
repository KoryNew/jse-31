package ru.tsk.vkorenygin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractTaskCommand;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.util.List;

public class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "task-show-by-project-id";
    }

    @Override
    public @Nullable String description() {
        return "show tasks by project id";
    }

    @Override
    public @NotNull Role @NotNull [] roles() {
        return new Role[] {Role.USER};
    }

    @Override
    public void execute() throws AbstractException {
        @NotNull final String currentUserId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LIST TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = serviceLocator.getProjectTaskService().findAllTasksByProjectId(id, currentUserId);
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
