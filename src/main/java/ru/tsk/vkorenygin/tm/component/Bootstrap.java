package ru.tsk.vkorenygin.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.api.service.*;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.system.UnknownArgumentException;
import ru.tsk.vkorenygin.tm.exception.system.UnknownCommandException;
import ru.tsk.vkorenygin.tm.repository.CommandRepository;
import ru.tsk.vkorenygin.tm.repository.ProjectRepository;
import ru.tsk.vkorenygin.tm.repository.TaskRepository;
import ru.tsk.vkorenygin.tm.repository.UserRepository;
import ru.tsk.vkorenygin.tm.service.*;
import ru.tsk.vkorenygin.tm.util.DataUtil;
import ru.tsk.vkorenygin.tm.util.SystemUtil;
import ru.tsk.vkorenygin.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Bootstrap implements IServiceLocator {

    @NotNull private final IPropertyService propertyService = new PropertyService();

    @NotNull private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull private final IUserRepository userRepository = new UserRepository();
    @NotNull private final IUserService userService = new UserService(userRepository, propertyService);
    @NotNull private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull private final ILogService logService = new LogService();

    @Nullable private String adminId = null;
    @Nullable private String userId = null;

    @NotNull
    private final Backup backup = new Backup(this);

    {
        initCommands(commandService.getCommandList());
        initUsers();
        initData();
        initPID();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (command == null)
            return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean parseArgs(@Nullable final String[] args) throws AbstractException {
        if (DataUtil.isEmpty(args))
            return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(@Nullable final String arg) throws AbstractException {
        if (DataUtil.isEmpty(arg))
            return;
        final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null)
            throw new UnknownArgumentException();
        command.execute();
    }

    public void parseCommand(@Nullable final String cmd) throws AbstractException {
        if (DataUtil.isEmpty(cmd))
            throw new UnknownCommandException();
        final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null)
            throw new UnknownCommandException();
        @NotNull final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

    public void run(@Nullable final String[] args) {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        backup.init();
        if (parseArgs(args))
            System.exit(0);
        process();
    }

    private void process() {
        logService.debug("Test environment.");
        @NotNull String command = "";
        while (true) {
            try {
                logService.command(command);
                 command = TerminalUtil.nextLine();
                parseCommand(command);
                logService.info("Commands '" + command + "' is executed.");
                System.out.println("[OK]");
            } catch (Exception e) {
                logService.error(e);
                System.out.println("[ERROR]");
            }
        }
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command : commandList) initCommand(command);
    }

    private void initCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initData() {
        try {
            projectService.create("Project C", "Desc 1", adminId);
            projectService.create("Project A", "Desc 2", userId);
            projectService.startByIndex(0, adminId);
            projectService.finishByIndex(0, userId);
            taskService.create("Task C", "Desc 1", adminId);
            taskService.create("Task A", "Desc 2", userId);
            taskService.finishByIndex(0, adminId);
            taskService.startByIndex(0, userId);
        } catch (RuntimeException e) {
            logService.error(e);
        }
    }

    private void initUsers() throws AbstractException {
        userId = userService.create("test", "test", "test@test.ru").getId();
        adminId = userService.create("admin", "admin", Role.ADMIN).getId();
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return authService;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

}
