package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.List;

public interface ICommandService {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArgs();

    @NotNull
    List<AbstractCommand> getCommandList();

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable final String name);

    void add(@Nullable final AbstractCommand command);

}
