package ru.tsk.vkorenygin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.AbstractEntity;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    E add(@NotNull final E entity);

    void addAll(@NotNull final Collection<E> collection);

    boolean existsById(@NotNull final String id);

    boolean existsByIndex(final int index);

    int getSize();

    @NotNull
    List<E> findAll();

    @NotNull
    List<E> findAll(@NotNull final Comparator<E> comparator);

    @NotNull
    Optional<E> findById(@NotNull final String id) throws EmptyIdException;

    @NotNull
    Optional<E> findByIndex(@NotNull final Integer index);

    void clear();

    @NotNull
    Optional<E> removeById(@NotNull final String id) throws EmptyIdException;

    @NotNull
    Optional<E> removeByIndex(@NotNull final Integer index);

    @NotNull
    Optional<E> remove(@NotNull final E entity);

}
