package ru.tsk.vkorenygin.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.entity.IWBS;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Date;

public class Task extends AbstractOwnerEntity implements IWBS {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private String projectId = null;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date startDate;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private Date createDate = new Date();

    public Task() {
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public @Nullable String getName() {
        return this.name;
    }

    @Override
    public void setName(final @NotNull String name) {
        this.name = name;
    }

    public @Nullable String getDescription() {
        return this.description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @Override
    public @NotNull Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(final @NotNull Status status) {
        this.status = status;
    }

    public @Nullable String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable String projectId) {
        this.projectId = projectId;
    }

    @Override
    public @Nullable Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(final @NotNull Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public @NotNull Date getCreateDate() {
        return createDate;
    }

    @Override
    public void setCreateDate(final @NotNull Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public @NotNull String toString() {
        return super.toString() +
                "Name: " + getName() + "; " +
                "Status: " + getStatus() + "; " +
                "Started: " + getStartDate() + "; " +
                "Created: " + getCreateDate() + "; ";
    }

}
