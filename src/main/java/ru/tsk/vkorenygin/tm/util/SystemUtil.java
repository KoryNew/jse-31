package ru.tsk.vkorenygin.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;

@UtilityClass
public class SystemUtil {

    private static final long EMPTY_PID = 0;

    public static long getPID() {
        @Nullable final String processName = ManagementFactory.getRuntimeMXBean().getName();
        if (DataUtil.isEmpty(processName)) return EMPTY_PID;
        try {
            return Long.parseLong(processName.split("@")[0]);
        } catch (Exception e) {
            return EMPTY_PID;
        }
    }

}
