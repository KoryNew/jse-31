package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.ICommandRepository;
import ru.tsk.vkorenygin.tm.api.service.ICommandService;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Collection;
import java.util.List;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public @NotNull Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public @NotNull Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @Override
    public @NotNull Collection<String> getCommandNames() {
        return commandRepository.getCommandNames();
    }

    @Override
    public @NotNull Collection<String> getCommandArgs() {
        return commandRepository.getCommandArgs();
    }

    @NotNull
    @Override
    public List<AbstractCommand> getCommandList() {
        return commandRepository.getCommandList();
    }

    @Override
    public @Nullable AbstractCommand getCommandByName(final @Nullable String name) {
        if (DataUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByName(name);
    }

    @Override
    public @Nullable AbstractCommand getCommandByArg(final @Nullable String name) {
        if (DataUtil.isEmpty(name)) return null;
        return commandRepository.getCommandByArg(name);
    }

    @Override
    public void add(final @Nullable AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

}
