package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IProjectRepository;
import ru.tsk.vkorenygin.tm.api.service.IProjectService;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.vkorenygin.tm.exception.enumerated.IncorrectStatusException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Optional;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void create(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public void create(final @Nullable String name, final @Nullable String description, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        projectRepository.add(project);
    }

    @Override
    public @NotNull Optional<Project> findByIndex(final int index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize())
            throw new ProjectNotFoundException();
        return projectRepository.findByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<Project> findByName(@Nullable final String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name))
            throw new EmptyNameException();
        return projectRepository.findByName(name, userId);
    }

    @Override
    public @NotNull Project changeStatusById(@Nullable final String id, @Nullable final Status status, @Nullable final String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new IncorrectStatusException();
        return projectRepository.changeStatusById(id, status, userId);
    }

    @Override
    public @NotNull Project changeStatusByName(final @Nullable String name, final @Nullable Status status, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new IncorrectStatusException();
        return projectRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public @NotNull Project changeStatusByIndex(final @Nullable Integer index, final @Nullable Status status, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize()) throw new ProjectNotFoundException();
        if (status == null) throw new IncorrectStatusException();
        return projectRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public @NotNull Project startById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.startById(id, userId);
    }

    @Override
    public @NotNull Project startByIndex(final @Nullable Integer index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize()) throw new ProjectNotFoundException();
        return projectRepository.startByIndex(index, userId);
    }

    @Override
    public @NotNull Project startByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.startByName(name, userId);
    }

    @Override
    public @NotNull Project finishById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.finishById(id, userId);
    }

    @Override
    public @NotNull Project finishByIndex(final @Nullable Integer index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > projectRepository.getSize()) throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index, userId);
    }

    @Override
    public @NotNull Project finishByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.finishByName(name, userId);
    }

    @Override
    public @NotNull Project updateByIndex(final @Nullable Integer index,
                                          final @Nullable String name,
                                          final @Nullable String description,
                                          final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        if (index - 1 > projectRepository.getSize()) throw new ProjectNotFoundException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();

        @NotNull Optional<Project> project = projectRepository.findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setDescription(description));
        return project.get();
    }

    @Override
    public @NotNull Project updateById(final @Nullable String id,
                                       final @Nullable String name,
                                       final @Nullable String description,
                                       final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();

        @NotNull Optional<Project> project = projectRepository.findById(id, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        project.ifPresent(e -> e.setName(name));
        project.ifPresent(e -> e.setName(description));
        return project.get();
    }

    @Override
    public @NotNull Optional<Project> removeByIndex(final int index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        @NotNull Optional<Project> project = projectRepository.findByIndex(index, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return projectRepository.removeByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<Project> removeByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull Optional<Project> project = projectRepository.findByName(name, userId);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return projectRepository.removeByName(name, userId);
    }

}
