package ru.tsk.vkorenygin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IOwnerRepository;
import ru.tsk.vkorenygin.tm.entity.AbstractOwnerEntity;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    protected Predicate<E> hasUser(final String userId) {
        return e -> userId.equals(e.getUserId());
    }

    public boolean existsById(final @NotNull String id, final @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .anyMatch(hasId(id));
    }

    public boolean existsByIndex(final int index, final @NotNull String userId) {
        if (index < 0) return false;
        return index < entities.size();
    }

    @Override
    public @NotNull Optional<E> remove(final @NotNull E entity, final @NotNull String userId) {
        if (!userId.equals(entity.getUserId())) return Optional.empty();
        entities.remove(entity);
        return Optional.of(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entities.stream()
                .filter(hasUser(userId))
                .forEach(e -> entities.remove(e));
    }

    @Override
    public @NotNull List<E> findAll(final @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .collect(Collectors.toList());
    }

    public @NotNull List<E> findAll(final @NotNull Comparator<E> comparator, @NotNull String userId) {
        return entities.stream()
                .filter(hasUser(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public @NotNull Optional<E> findById(final @NotNull String id, final @NotNull String userId) throws AbstractException {
        return entities.stream()
                .filter(hasUser(userId))
                .filter(hasId(id))
                .findFirst();
    }

    @Override
    public @NotNull Optional<E> findByIndex(final int index, final @NotNull String userId) throws AbstractException {
        return entities.stream()
                .filter(hasUser(userId))
                .skip(index)
                .findFirst();
    }

    @Override
    public @NotNull Optional<E> removeById(final @NotNull String id, final @NotNull String userId) throws AbstractException {
        @Nullable final Optional<E> entity = findById(id, userId);
        entity.ifPresent(e -> entities.remove(e));
        return entity;
    }

    @Override
    public @NotNull Optional<E> removeByIndex(final int index, final @NotNull String userId) throws AbstractException {
        @Nullable final Optional<E> entity = findByIndex(index, userId);
        entity.ifPresent(e -> entities.remove(e));
        return entity;
    }

}
