package ru.tsk.vkorenygin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.IUserRepository;
import ru.tsk.vkorenygin.tm.entity.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    protected Predicate<User> hasLogin(String login) {
        return e -> login.equals(e.getLogin());
    }

    protected Predicate<User> hasEmail(String email) {
        return e -> email.equals(e.getEmail());
    }

    @Override
    public @NotNull Optional<User> findByLogin(final @NotNull String login) {
        return entities.stream()
                .filter(hasLogin(login))
                .findFirst();
    }

    @Override
    public @NotNull Optional<User> findByEmail(final @NotNull String email) {
        return entities.stream()
                .filter(hasEmail(email))
                .findFirst();
    }

    @Override
    public @NotNull Optional<User> removeByLogin(final @NotNull String login) {
        @Nullable final Optional<User> user = findByLogin(login);
        user.ifPresent(e -> entities.remove(e));
        return user;
    }

}
