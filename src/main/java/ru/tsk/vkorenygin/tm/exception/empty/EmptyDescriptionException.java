package ru.tsk.vkorenygin.tm.exception.empty;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("Error! Description is empty.");
    }

}
